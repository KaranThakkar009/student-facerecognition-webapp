const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const morgan = require('morgan');
const cors = require('cors');
const jsonData = require('../src/descriptors/bnk48.json')

const app = express();
const port = 5000;
const hostname = '192.168.43.113';

app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(cors());

app.get('/',(req,res) => res.status(200).send({
    message: "Server is running..."
}));


const WriteTextToFileSync =  (contentToWrite) => {
    fs.writeFileSync('./src/descriptors/bnk48.json',contentToWrite,(err) =>{
        //console.log(contentToWrite);
        if(err) {
            console.log(err);
        }else {
            console.log('Done writing to file successfully...')
        }
    })
}
const user = {

}
app.post('/write',(req,res,next) =>{
    const user = {
        "name": req.body[0].name,
        "descriptors": req.body[0].descriptors
    }
    jsonData[user.name]=user
    //console.log(req.body[0].descriptors)
    const requestContent = JSON.stringify(jsonData,null,2);
    WriteTextToFileSync(requestContent)
});

app.use((req,res,next) => res.status(404).send({
    message: "Couldn't find specified route that was requested..."
}));

app.listen(port,hostname,()=>{
    console.log(
        `
        !!! server is running..
        !!! Listening for incoming requests on port ${port}
        !!! Server running at http://${hostname}:${port}/
        !!! http://localhost:5000
        `
    )
})